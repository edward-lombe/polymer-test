# Polymer Test
---

Simple example of a Polymer use case. To get started clone the repository and
then serve the entire folder, this can be done easily with `http-server .`
which can be installed through `npm install -g http-server`.
